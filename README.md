##### Задание
Написать простую страницу с возможность добавлять пользователя (Имя, Фамилия, Отчество) в список пользователей, а так же редактировать и удалять. Желательно для хранения данных использовать redux.

##### Описание:
Страница со списком пользователей (Имя, Фамилия, Отчество), слева кнопка удалить, изменить на против каждого элемента.
Список должен поддерживать пагинацию. При изменении открывается форма редактирования элемента. При добавлении открывается форма редактирования элемента. Все поля обязательны к заполнению. Если поле не заполнено, подсвечивать его подсказывать что его нужно заполнить.

